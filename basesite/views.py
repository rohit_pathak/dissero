from django.shortcuts import render
from django.views import View


class Index(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, 'dissero-app.html')
        return render(request, 'index.html')
