import mimetypes
import os
import sys

import boto3

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
BASE_ASSETS_DIR = os.path.join(BASE_DIR, 'webapp', 'www', 'assets')
AWS_PROFILES = {
    'dissero-pilot': {'bucket': 'dissero-pilot'},
    'dissero-prod': {'bucket': 'dissero-prod'}
}


def get_all_asset_paths(path):
    files = []
    for (dirpath, dirnames, filenames) in os.walk(path):
        files += [os.path.join(dirpath, f) for f in filenames]
        for d in dirnames:
            sub_files = get_all_asset_paths(os.path.join(path, d))
            files += sub_files
    return files


def aws_asset_path(absolute_path):
    return 'assets' + absolute_path.partition(BASE_ASSETS_DIR)[2]


if __name__ == '__main__':
    aws_profile = sys.argv[1]
    if aws_profile not in AWS_PROFILES:
        print('Not a valid profile. Valid profiles are ' + ', '.join(AWS_PROFILES.keys()))
        sys.exit(1)

    print('Using AWS profile:', aws_profile)
    session = boto3.Session(profile_name=aws_profile)
    s3 = session.client('s3')

    assets = get_all_asset_paths(BASE_ASSETS_DIR)
    print(len(assets))
    for a in assets:
        print('Uploading asset to', aws_asset_path(a))
        mime_type = mimetypes.guess_type(a)[0]
        if mime_type is not None:
            extra_args = {'ContentType': mime_type}
        else:
            extra_args = {}
        s3.upload_file(a, AWS_PROFILES[aws_profile]['bucket'],
                       aws_asset_path(a), ExtraArgs=extra_args)
