from django.conf import settings
from django.contrib.postgres.search import SearchVectorField
from django.db import models

from classroom.models import Classroom


# TODO: when saving, validate if users are indeed in the classroom

class Conversation(models.Model):
    QUESTION = 'QUESTION'
    GENERAL = 'GENERAL'
    CATEGORY_CHOICES = (
        (QUESTION, 'Question'),
        (GENERAL, 'General')
    )
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE)
    category = models.CharField(max_length=20, choices=CATEGORY_CHOICES)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    modified_timestamp = models.DateTimeField(auto_now=True)

    @property
    def title(self, ):
        if self.discussion_set.filter(is_main=True).exists():
            return self.discussion_set.get(is_main=True).title
        return ''

    @property
    def details(self, ):
        if self.discussion_set.filter(is_main=True).exists():
            return self.discussion_set.get(is_main=True).details
        return ''

    def __str__(self):
        return '%s (%s) | %s' % (self.title, self.author, self.classroom)


class Discussion(models.Model):
    QUESTION = 'QUESTION'
    GENERAL = 'GENERAL'
    CATEGORY_CHOICES = (
        (QUESTION, 'Question'),
        (GENERAL, 'General')
    )
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    conversation = models.ForeignKey(Conversation, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    details = models.TextField(blank=True, null=True)
    category = models.CharField(max_length=20, choices=CATEGORY_CHOICES)
    is_main = models.BooleanField(default=False)
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='liked_discussions', blank=True)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    modified_timestamp = models.DateTimeField(auto_now=True)
    search_vector = SearchVectorField(null=True)

    def __str__(self):
        return '%s (%s)' % (self.title, self.author)


class Message(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    discussion = models.ForeignKey(Discussion, on_delete=models.CASCADE)
    text = models.TextField()
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='liked_messages', blank=True)
    is_answer = models.BooleanField(default=False)
    is_by_instructor = models.BooleanField(default=False)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    modified_timestamp = models.DateTimeField(auto_now=True)
    search_vector = SearchVectorField(null=True)

    @property
    def is_liked_by_instructor(self, ):
        # TODO: explore using filter expressions or storing this as a field directly on the table
        return self.likes.filter(pk__in=self.discussion.conversation.classroom.instructors.values('pk').all()).exists()

    def __str__(self):
        return '%s (%s) | %s' % (self.text[:20], self.author, self.discussion)
