from django.contrib import admin

# Register your models here.
from conversation.models import Discussion, Conversation, Message

admin.site.register(Conversation)
admin.site.register(Discussion)
admin.site.register(Message)
