from rest_framework import serializers

from conversation.models import Conversation, Discussion, Message
from useraccount.api.serializers import UserSerializer


class MessageSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)

    class Meta:
        model = Message
        fields = ('id', 'author', 'discussion', 'text', 'likes', 'is_answer', 'is_by_instructor',
                  'is_liked_by_instructor', 'created_timestamp', 'modified_timestamp')


class DiscussionSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)
    messages = MessageSerializer(many=True, read_only=True, source='message_set')

    class Meta:
        model = Discussion
        fields = ('id', 'author', 'conversation', 'title', 'details', 'category', 'messages',
                  'is_main', 'likes', 'created_timestamp', 'modified_timestamp')
        read_only_fields = ('messages', 'is_main', 'created_timestamp', 'modified_timestamp')


class ConversationSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)
    discussions = DiscussionSerializer(many=True, read_only=True, source='discussion_set')

    class Meta:
        model = Conversation
        fields = ('id', 'author', 'classroom', 'title', 'details', 'category', 'discussions',
                  'created_timestamp', 'modified_timestamp')
        read_only_fields = ('discussions', 'created_timestamp', 'modified_timestamp')


class ConversationSummarySerializer(serializers.ModelSerializer):
    """
    Serializer used to return a summary of a conversation and to create new conversations.
    """
    author = UserSerializer(read_only=True)
    title = serializers.CharField(read_only=False)
    details = serializers.CharField(read_only=False)

    def create(self, validated_data):
        title = validated_data.pop('title')
        details = validated_data.pop('details')
        conversation = Conversation.objects.create(**validated_data)
        conversation.discussion_set.create(conversation=conversation, title=title,
                                           details=details, category=conversation.category,
                                           author=conversation.author, is_main=True)
        return conversation

    # TODO: include 'highlights'
    class Meta:
        model = Conversation
        fields = ('id', 'author', 'classroom', 'title', 'details', 'category',
                  'created_timestamp', 'modified_timestamp')
        read_only_fields = ('created_timestamp', 'modified_timestamp')
