from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('', views.ConversationList.as_view(), name='conversation-list'),
    path('<int:pk>/', views.ConversationDetail.as_view(), name='conversation-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)