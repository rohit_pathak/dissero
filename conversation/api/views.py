from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from classroom.api.permissions import classroom_member_or_forbidden
from conversation.api.permissions import IsOwnerOrReadOnly
from conversation.api.serializers import ConversationSerializer, ConversationSummarySerializer
from conversation.models import Conversation


class ConversationList(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ConversationSummarySerializer
    queryset = Conversation.objects.all()

    def perform_create(self, serializer):
        classroom_member_or_forbidden(self.request.user, serializer.validated_data.get('classroom'))
        serializer.save(author=self.request.user)


class ConversationDetail(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly)
    serializer_class = ConversationSerializer
    queryset = Conversation.objects.all()

    def retrieve(self, request, *args, **kwargs):
        conversation = self.get_object()
        classroom_member_or_forbidden(request.user, conversation.classroom)
        return super().retrieve(request, *args, **kwargs)
