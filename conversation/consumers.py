import json

from asgiref.sync import async_to_sync
from channels.exceptions import StopConsumer
from django.core.exceptions import ObjectDoesNotExist

from classroom.models import Classroom
from conversation.api.serializers import DiscussionSerializer, MessageSerializer, ConversationSummarySerializer
from conversation.models import Conversation
from conversation.websocketresources import WebsocketResourceConsumer


def disconnect_if_not_in_classroom(user, classroom):
    if not classroom.has_member(user):
        print('User %s is not in classroom %s' % (user, classroom))
        raise StopConsumer()


class ConversationConsumer(WebsocketResourceConsumer):  # TODO: make this an async consumer
    """
    Consumer for users when they are in a conversation.
    Manages new discussions and messages in those discussions
    """
    resource_serializers = {
        'discussions': DiscussionSerializer,
        'messages': MessageSerializer,
    }
    resource_message_type = {
        'discussions': 'conversation_discussion',
        'messages': 'conversation_message',
    }

    def connect(self):
        if not self.scope['user'].is_authenticated:
            raise StopConsumer()

        self.user = self.scope['user']
        self.conversation_id = self.scope['url_route']['kwargs']['conversation_id']
        self.conversation_group_name = 'conversation_%s' % self.conversation_id

        try:
            conversation = Conversation.objects.get(pk=self.conversation_id)
            disconnect_if_not_in_classroom(self.user, conversation.classroom)
        except ObjectDoesNotExist:
            print("No conversation found with id %s" % self.conversation_id)
            raise StopConsumer()

        # Join conversation group (i.e. a group of consumers in this conversation)
        async_to_sync(self.channel_layer.group_add)(
            self.conversation_group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        print('disconnected')
        async_to_sync(self.channel_layer.group_discard)(
            self.conversation_group_name,
            self.channel_name
        )

    def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        print('received conversation data:', data)
        if self.is_resource_request(data):
            self.save_and_broadcast(self.conversation_group_name, data)
        else:
            print('not a resource request')

    def perform_create(self, serializer):
        serializer.save(author=self.user)

    def conversation_message(self, event):
        message = event['message']
        print('sending message to my client', message)
        # Send message to WebSocket
        self.send(text_data=json.dumps(message))

    def conversation_discussion(self, event):
        message = event['message']
        self.send(text_data=json.dumps(message))


class ConversationListConsumer(WebsocketResourceConsumer):
    """
    Consumer for the conversation list view.
    Manages new conversations that are created.
    """
    resource_serializers = {
        'conversations': ConversationSummarySerializer,
    }
    resource_message_type = {
        'conversations': 'classroom_conversation',
    }

    def connect(self):
        if not self.scope['user'].is_authenticated:
            raise StopConsumer()

        self.user = self.scope['user']
        self.classroom_id = self.scope['url_route']['kwargs']['classroom_id']
        self.classroom_group_name = 'classroom_%s' % self.classroom_id

        try:
            classroom = Classroom.objects.get(pk=self.classroom_id)
            disconnect_if_not_in_classroom(self.user, classroom)
        except ObjectDoesNotExist:
            print("No classroom found with id %s" % self.classroom_id)
            raise StopConsumer()

        async_to_sync(self.channel_layer.group_add)(
            self.classroom_group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        print('disconnected')
        async_to_sync(self.channel_layer.group_discard)(
            self.classroom_group_name,
            self.channel_name
        )

    def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        print('received classroom data:', data)
        if self.is_resource_request(data):
            self.save_and_broadcast(self.classroom_group_name, data)

    def perform_create(self, serializer):
        classroom = serializer.validated_data.get('classroom')
        if not classroom.has_member(self.user):
            print('not in classroom')
            self.send(json.dumps({'status_code': 'unauthorized'}))
            raise StopConsumer()
        serializer.save(author=self.user)

    def classroom_conversation(self, event):
        message = event['message']
        self.send(text_data=json.dumps(message))
