import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer


class WebsocketResourceConsumer(WebsocketConsumer):  # TODO: consider making this open source
    """
    Receives data to create, update, or delete resources.
    Resource serializers must inherit from DRF ModelSerializer.
    If an update is requested, 'id' is mandatory in the payload.

    For example, for a 'Discussion', text_data looks like:
    {
        "resource": "discussions",
        "action": "create",
        "payload": {
            "id": 23, (optional)
            "title": "Awesome discussion title!"
            "content": "Awesome discussion content",
            "conversation": 1
        }
    }
    """

    resource_serializers = {}
    resource_message_type = {}

    def is_resource_request(self, data):
        valid = 'resource' in data and 'action' in data and 'payload' in data
        valid = valid and data['resource'] in self.resource_serializers
        valid = valid and data['action'] in ('create', 'update', 'delete')
        if data['action'] == 'update' or data['action'] == 'delete':
            valid = valid and 'id' in data['payload']
        return valid

    def get_serializer(self, data):
        resource, action, payload = data['resource'], data['action'], data['payload']
        if action == 'create':
            serializer = self.resource_serializers[resource](data=payload)
            return serializer
        elif action == 'update':
            # TODO: design this properly; this has many assumptions
            instance = self.resource_serializers[resource].Meta.model.objects.get(pk=payload['id'])
            serializer = self.resource_serializers[resource](instance, data=payload)
            return serializer

    def save_and_broadcast(self, group_name, data):
        serializer = self.get_serializer(data)
        if not serializer.is_valid():
            self.send(json.dumps({
                'error': 'invalid data',
                'details': serializer.errors
            }))
        else:
            if data['action'] == 'create':
                self.perform_create(serializer)
            elif data['action'] == 'update':
                self.perform_update(serializer)
            async_to_sync(self.channel_layer.group_send)(
                group_name,
                {
                    'type': self.resource_message_type[data['resource']],
                    'message': {
                        'resource': data['resource'],
                        'action': data['action'],
                        'payload': serializer.data,
                    }
                }
            )

    def perform_create(self, serializer):
        serializer.save()

    def perform_update(self, serializer):
        serializer.save()
