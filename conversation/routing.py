from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/classrooms/(?P<classroom_id>[^/]+)/conversations/$', consumers.ConversationListConsumer),
    url(r'^ws/conversations/(?P<conversation_id>[^/]+)/$', consumers.ConversationConsumer),
]
