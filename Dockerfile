FROM python:3.6-alpine

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install dependencies for psycopg2
RUN apk add --no-cache gcc musl-dev python3-dev postgresql-dev

# Install app dependencied specified in requirements.txt
ADD requirements.txt /requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 3000 available to the world outside this container
EXPOSE 3000

# Define environment variables
ENV PYTHONPATH $PYTHONPATH:/app/

ENTRYPOINT ["/app/init.sh"]
