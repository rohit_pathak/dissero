#!/bin/sh

python manage.py makemigrations && \
python manage.py migrate && \
python manage.py initsuperuser --email admin@dissero.co --password dissero123 && \
daphne -b 0.0.0.0 -p 3000 dissero.asgi:application