"""dissero URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

import basesite.views
import useraccount.views

urlpatterns = [
    path('', basesite.views.Index.as_view(), name='home-page'),
    path('admin/', admin.site.urls),
    path('login/', useraccount.views.login_request, name='login-page'),
    path('logout/', useraccount.views.logout_request, name='logout-page'),
    path('dashboard/', useraccount.views.dashboard, name='dashboard-page'),

    path('accounts/', include('registration.backends.simple.urls')),
    path('api/users/', include('useraccount.api.urls')),

    path('classrooms/', include('classroom.urls')),
    path('api/classrooms/', include('classroom.api.urls')),
    
    path('api/conversations/', include('conversation.api.urls')),
]
