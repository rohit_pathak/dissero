from .settings import *

DEBUG = False
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '.dissero.co', '157.230.81.108']

AWS_LOCATION = 'assets'
AWS_STORAGE_BUCKET_NAME = 'dissero-pilot'

# Staticfiles settings for S3
STATIC_URL = 'https://%s.s3.amazonaws.com/%s/' % (AWS_STORAGE_BUCKET_NAME, AWS_LOCATION)

# Django Channels Settings
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('channel_layer', 6379)],
        },
    },
}

# Postgres DB settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dissero',
        'USER': 'disseroapp',
        'PASSWORD': os.environ.get('DATABASE_PASSWORD', ''),
        'HOST': 'database',
        'PORT': '',
    }
}

