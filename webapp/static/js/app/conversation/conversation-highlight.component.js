(function () {
    angular.module('dissero')
        .component('conversationHighlight', {
            controllerAs: 'vm',
            bindings: {
                highlight: '<'
            },
            template: `
            <div class="mb-1">
                <h6 class="mb-0"><i class="fas fa-fw" ng-class="vm.icon"></i> {{ vm.highlight.text }}</h6>
            </div>
            `,
            controller: function () {
                let vm = this;
                vm.icon = '';

                vm.$onInit= function () {
                    vm.icon = vm.highlight.type === 'QUESTION' ? 'fa-question-square text-warning' : 'fa-pen-alt text-warning';
                }

            }
        })
})();