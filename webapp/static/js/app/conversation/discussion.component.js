(function () {
    angular.module('dissero')
        .component('discussion', {
            controllerAs: 'vm',
            bindings: {
                discussion: '<',
                onUpdateDiscussion: '&'
            },
            require: {
                classroomCtrl: '^^classroom',
                conversationCtrl: '^^conversation'
            },
            template: `
                <div id="discussion-{{vm.discussion.id}}" class="discussion card mb-4" ng-class="vm.discussion.is_main ? 'border-dark' : 'bg-light'">
                    <div class="card-body p-2">
                        <div class="font-weight-bold text-muted">
                            <small>{{ vm.discussion.category | uppercase }}</small>
                            <a ng-if="vm.discussion.author.id === vm.currentUser.id && !vm.showEditForm" href="" title="Edit"
                               ng-click="vm.editDiscussion()" class="float-right"><i class="fas fa-edit"></i></a>
                        </div>
                        <div class="mb-3"><small><em>{{ vm.discussion.author.full_name }} on {{ vm.discussion.created_timestamp | date: 'mediumDate'}}</em></small></div>
                        <div ng-if="!vm.showEditForm">
                            <h4 class="card-title">{{ vm.discussion.title }}</h4>
                            <div ng-bind-html="vm.discussion.details" class="trix-content"></div>                  
                        </div>
                        <div ng-if="vm.showEditForm">
                            <form ng-submit="vm.saveDiscussion()">
                                <div><small>Title</small></div>
                                <input ng-model="vm.editCopy.title" class="form-control mb-3" required>
                                <div><small>Details</small></div>
                                <trix-editor angular-trix ng-if="true" ng-model="vm.editCopy.details" 
                                             class="trix-content" placeholder="Details..." required></trix-editor>
                                <div class="mt-2">
                                    <button type="submit" class="btn btn-success">save</button>
                                    <button class="btn btn-secondary" ng-click="vm.cancelEdit()">Cancel</button>
                                </div>
                            </form>
                        </div>
                        <h5 class="text-muted mt-3"><likes users="vm.discussion.likes" current-user="vm.currentUser" 
                                   on-like="vm.likeDiscussion(likedBy)" on-unlike="vm.unlikeDiscussion(unlikedBy)"></likes></h5>
                        <hr>
                        <message ng-repeat="m in vm.discussion.messages | orderBy:'created_timestamp'"
                                 is-main="vm.discussion.is_main"
                                 message="m"
                                 can-be-accepted="vm.discussion.category === 'QUESTION' && vm.discussion.author.id === vm.currentUser.id"
                                 on-update="vm.updateMessage(message)"></message>
                        <form ng-submit="vm.submitMessage(d)" class="mt-2">
                            <trix-editor angular-trix ng-if="true" ng-model="vm.newMessage.text" placeholder="Reply..." required></trix-editor>
                            <button type="submit" class="btn btn-xs btn-success mt-1"><i class="far fa-paper-plane"></i> Reply</button>
                        </form>
                    </div>
                </div>
            `,
            controller: ['userService', function (userService) {
                let vm = this;
                vm.newMessage = {};
                vm.currentUser = null;
                vm.showEditForm = false;
                vm.editCopy = null;

                vm.submitMessage = function () {
                    vm.newMessage.discussion = vm.discussion.id;
                    let data = {
                        'resource': 'messages',
                        'action': 'create',
                        'payload': vm.newMessage
                    };
                    console.log(data);
                    vm.conversationCtrl.socket.send(data);
                    vm.newMessage = {};
                };

                vm.updateMessage = function (message) {
                    let data = {
                        'resource': 'messages',
                        'action': 'update',
                        'payload': message
                    };
                    vm.conversationCtrl.socket.send(data);
                };

                vm.likeDiscussion = function (userId) {
                    console.log('liking discussion');
                    let likedBy = vm.discussion.likes.find(l => l === userId);
                    if (likedBy === undefined) vm.discussion.likes.push(userId);
                    vm.onUpdateDiscussion({'discussion': vm.discussion});
                };

                vm.unlikeDiscussion = function (userId) {
                    console.log('unliking discussion');
                    let unlikedByIndex = vm.discussion.likes.indexOf(userId);
                    if (unlikedByIndex >= 0) {
                        let updatedDiscussion = _.cloneDeep(_.pick(vm.discussion,
                            'id', 'conversation', 'title', 'details', 'category', 'likes',));
                        updatedDiscussion.likes.splice(unlikedByIndex, 1);
                        vm.onUpdateDiscussion({'discussion': updatedDiscussion});
                    }
                };

                vm.editDiscussion = function() {
                    vm.editCopy = _.cloneDeep(_.pick(vm.discussion,
                            'id', 'conversation', 'title', 'details', 'category', 'likes',));
                    vm.showEditForm = true;
                };

                vm.cancelEdit = function() {
                    vm.showEditForm = false;
                };

                vm.saveDiscussion = function() {
                    console.log(vm.editCopy);
                    // TODO: show errors if this fails
                    vm.onUpdateDiscussion({'discussion': vm.editCopy});
                    _.assign(vm.discussion, vm.editCopy);
                    vm.showEditForm = false;
                    vm.editCopy = null;
                };

                vm.$onChanges = function (changes) {
                    if (changes.discussion.currentValue != null) {
                        vm.discussion = changes.discussion.currentValue;
                    }
                };

                vm.$onInit = function () {
                    userService.getCurrentUser().then(function (user) {
                        vm.currentUser = user;
                    });
                }
            }]
        });
})();