(function () {
    angular.module('dissero')
        .component('conversationList', {
            controllerAs: 'vm',
            bindings: {
                classroomId: '<'
            },
            template: `
            <div ng-show="!vm.showNewConversationForm" class="row mb-2">
                <div class="col-sm-1 mb-1">
                    <a ng-click="vm.showNewConversationForm = true" class="btn btn-success btn-block btn-lg" href=""><i class="fas fa-plus"></i></a>
                </div>
                <div class="col-sm-11">
                    <form ng-submit="vm.search()">
                        <div class="input-group">
                            <input type="text" ng-model="vm.searchTerm" class="form-control form-control-lg" placeholder="Search..." required>
                            <div class="input-group-append">
                                <button class="btn btn-outline-dark" type="submit"><i class="fas fa-search ml-2 mr-2"></i></button>
                            </div>
                        </div>
                    </form>
                    
                    <div class="mt-2" ng-show="vm.searchResults">
                        <h6>Search results ({{ vm.searchResults.length }}) <small><a href="" class="text-danger" ng-click="vm.clearSearch()">CLEAR</a></small></h6>
                        <div ng-repeat="sr in vm.searchResults" class="card mb-2">
                            <div class="card-body p-2">
                                <h6 class="card-title pb-0 mb-1"><small>{{ sr.category | uppercase }}</small></h6>
                                <a ui-sref="classroom.conversation({conversationId: sr.conversation_id, messageId: sr.message_id, discussionId: sr.discussion_id})" href>{{ sr.text }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div ng-show="vm.showNewConversationForm" class="card p-2 mb-3">
                <h5 class="pb-1 mb-2">New Conversation</h5>
                <form ng-submit="vm.createConversation()">
                    <div><small>Type</small></div>
                    <div class="form-group mb-3">
                        <div class="custom-control custom-radio">
                          <input type="radio" id="conversation-category-question" name="category" 
                                 ng-model="vm.newConversation.category" value="QUESTION" 
                                 class="custom-control-input" required>
                          <label class="custom-control-label" for="conversation-category-question">Question</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input type="radio" id="conversation-category-open" name="category" 
                                 ng-model="vm.newConversation.category" value="GENERAL" 
                                 class="custom-control-input" required>
                          <label class="custom-control-label" for="conversation-category-open">Open Discussion</label>
                        </div>
                    </div>
                    <div><small>Title</small></div>
                    <input ng-model="vm.newConversation.title" class="form-control mb-3" required>
                    <div><small>Details</small></div>
                    <trix-editor angular-trix ng-model="vm.newConversation.details" placeholder="Details..." required></trix-editor>
                    <div class="mt-2">
                        <button type="submit" class="btn btn-success">Create</button>
                        <button class="btn btn-secondary" ng-click="vm.showNewConversationForm = false">Cancel</button>
                    </div>
                </form>
            </div>
            
            <conversation-summary ng-show="!vm.searchResults" ng-repeat="c in vm.conversations | orderBy:'-created_timestamp'" 
                                  conversation="c"></conversation-summary>
            `,
            controller: ['$websocket', '$location', 'conversationService', 'classroomService', function ($websocket, $location, conversationService, classroomService) {
                let vm = this;
                vm.conversations = [];
                vm.newConversation = {};
                vm.conversationMap = {};
                vm.showNewConversationForm = false;
                vm.searchTerm = null;
                vm.searchResults = null;
                let stream = null;

                vm.createConversation = function () {
                    vm.newConversation.classroom = parseInt(vm.classroomId);
                    let data = {
                        'resource': 'conversations',
                        'action': 'create',
                        'payload': vm.newConversation
                    };
                    stream.send(data);
                    vm.newConversation = {classroom: vm.classroomId};
                    vm.showNewConversationForm = false;
                };

                vm.processSocketData = function (data) {
                    console.log('Processing socket data');
                    console.log(data);
                    if ('resource' in data && data.resource === 'conversations') {
                        if (data.action === 'create') {
                            let conversation = data.payload;
                            vm.conversations.unshift(data.payload);
                            vm.conversationMap[conversation.id] = conversation;
                        }
                    }
                };

                vm.search = function () {
                    classroomService.search(vm.classroomId, vm.searchTerm).then(function (data) {
                        console.log(data);
                        vm.searchResults = data;
                    });
                };

                vm.clearSearch = function() {
                    vm.searchTerm = '';
                    vm.searchResults = null;
                };

                vm.$onInit = function () {
                    console.log('Initializing conversation list');
                    conversationService.getAllConversations(vm.classroomId).then(function (data) {
                        vm.conversations = data;
                        vm.conversationMap = _.groupBy(vm.conversations, c => c.id);
                        console.log(vm.conversationMap);
                        const scheme = $location.protocol() === 'https' ? 'wss' : 'ws';
                        const url = `${scheme}://${$location.host()}:${$location.port()}/ws/classrooms/${vm.classroomId}/conversations/`;
                        stream = $websocket(url);
                        stream.onMessage(function (rawMessage) {
                            let data = JSON.parse(rawMessage.data);
                            vm.processSocketData(data);
                        });
                    });
                };

                vm.$onDestroy = function () {
                    stream.close(true);
                }
            }]
        });
})();