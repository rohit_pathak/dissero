(function () {
    angular.module('dissero')
        .component('conversationSummary', {
            controllerAs: 'vm',
            bindings: {
                conversation: '<'
            },
            template: `
            <div class="card mb-2">
                <div class="card-body p-3">
                    <div class="font-weight-bold text-muted"><small>{{ vm.conversation.category | uppercase }}</small></div>
                    <div class="mb-3"><small><em>{{ vm.conversation.author.full_name }} on {{ vm.conversation.created_timestamp | date: 'mediumDate'}}</em></small></div>
                    <h5 class="card-title pb-0 mb-1">
                        <div class="float-right text-muted ml-2"><i class="fas fa-user-friends"></i> <small>{{ vm.online }}</small></div>
                        <a ui-sref="classroom.conversation({conversationId: vm.conversation.id})" href>{{ vm.conversation.title }}</a>
                    </h5>
                    <div ng-bind-html="vm.conversation.details" class="trix-content"></div> <!-- TODO: truncate -->
                    <div ng-show="vm.showHighlights">
                        <conversation-highlight ng-repeat="h in vm.conversation.highlights" highlight="h"></conversation-highlight>
                    </div>
                    <div ng-show="vm.conversation.highlights">
                        <a href="" ng-click="vm.showHighlights = true" ng-show="!vm.showHighlights">Show highlights</a>
                        <a href="" ng-click="vm.showHighlights = false" ng-show="vm.showHighlights">Hide highlights</a>
                    </div>
                </div>
            </div>
            `,
            controller: ['conversationService', function (conversationService) {
                let vm = this;
                vm.showHighlights = false;
                vm.online = Math.floor(Math.random() * 15);
            }]
        });
})();