(function () {
    angular.module('dissero')
        .component('likes', {
            controllerAs: 'vm',
            bindings: {
                users: '<',
                currentUser: '<',
                onLike: '&',
                onUnlike: '&'
            },
            template: `
                <a href="" ng-show="!vm.isLiked" ng-click="vm.like()" 
                   class="text-warning"><i class="far fa-heart fa-fw"></i></a>
                <a href="" ng-show="vm.isLiked" ng-click="vm.unlike()" 
                   class="text-warning"><i class="fas fa-heart fa-fw"></i></a>
                <small>({{ vm.users.length }} likes)</small>
            `,
            controller: function () {
                let vm = this;
                let prevUsers = null;
                vm.isLiked = false;

                vm.like = function () {
                    vm.onLike({'likedBy': vm.currentUser.id});
                };

                vm.unlike = function() {
                    vm.onUnlike({'unlikedBy': vm.currentUser.id});
                };

                vm.$onInit = function () {
                    prevUsers = _.clone(vm.users);
                    vm.isLiked = vm.users.find(u => vm.currentUser !== null && vm.currentUser.id === u) !== undefined;
                };

                vm.$doCheck = function () { // hooks into the digest cycle and updates the isLiked state
                    let currentUsers = vm.users;
                    // TODO: make it more efficient (O(n) is not cool)
                    if (!(prevUsers.length === currentUsers.length === _.intersection(prevUsers, currentUsers).length)) {
                        prevUsers = _.clone(currentUsers);
                        vm.isLiked = vm.users.find(u => vm.currentUser !== null && vm.currentUser.id === u) !== undefined;
                    }
                }
            }
        });
})();