(function () {
    angular.module('dissero')
        .factory('conversationService', ['$http', function ($http) {
            let factory = {};

            factory.getAllConversations = function (classroomId) {
                return $http.get('/api/classrooms/' + classroomId + '/conversations').then(function (response) {
                    return response.data;
                });
            };

            factory.getConversationSummary = function (conversationId) {
                return {
                    id: conversationId,
                    title: 'What is the meaning of concept ' + conversationId + ' and how do I solve equation ' + Math.random() + '?',
                    highlights: [1, 2, 3, 4].map(q => {
                        return {
                            id: q,
                            text: 'Why is ' + q + ' not the right answer?',
                            type: (q !== 3 ? 'QUESTION' : 'IMPORTANT'),
                        }
                    })
                }
            };

            factory.getConversation = function (conversationId) {
                return $http.get('/api/conversations/' + conversationId).then(function (response) {
                    return response.data;
                });
            };

            return factory;
        }]);
})();