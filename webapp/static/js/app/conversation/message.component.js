(function () {
    angular.module('dissero')
        .component('message', {
            controllerAs: 'vm',
            bindings: {
                message: '<',
                isMain: '<',
                canBeAccepted: '<',
                onUpdate: '&'
            },
            require: {
                discussionCtrl: '^^discussion'
            },
            template: `
            <div id="message-{{vm.message.id}}" class="p-2 mb-1" ng-class="{
                    'bg-light': vm.isMain,
                    'bg-white': !vm.isMain,
                    'border border-success': vm.message.is_answer,
                    'border border-warning': !vm.message.is_answer && vm.message.is_liked_by_instructor 
            }">
                <div class="small">
                    <em>{{ vm.message.author.full_name }} on {{ vm.message.created_timestamp | date: 'MMM d, y h:mm a'}}</em>
                    <div class="float-right">
                        <a href="" class="mr-1 text-success" 
                           ng-show="vm.canBeAccepted && !vm.message.is_answer"
                           ng-click="vm.acceptAsAnswer(true)"><i class="far fa-check-square fa-fw"></i></a>
                        <a href="" class="pr-1 text-success" 
                           ng-show="vm.canBeAccepted && vm.message.is_answer"
                           ng-click="vm.acceptAsAnswer(false)"><i class="fas fa-check-square fa-fw"></i></a>
                        <likes users="vm.message.likes" current-user="vm.currentUser" 
                               on-like="vm.likeMessage(likedBy)"
                               on-unlike="vm.unlikeMessage(unlikedBy)"></likes>
                    </div>
                </div>
                <div ng-bind-html="vm.message.text" class="trix-content"></div>
            </div>
            `,
            controller: ['userService', function (userService) {
                let vm = this;
                vm.currentUser = null;

                vm.likeMessage = function (userId) {
                    console.log('liking message', vm.message);
                    let likedBy = vm.message.likes.find(u => u === userId);
                    if (likedBy === undefined) {
                        let updatedMessage = _.cloneDeep(_.pick(vm.message, 'id', 'discussion',
                            'text', 'likes', 'is_answer'));
                        updatedMessage.likes.push(userId);
                        vm.onUpdate({'message': updatedMessage});
                    }
                };

                vm.unlikeMessage = function(userId) {
                    console.log('unliking message');
                    let unlikedByIndex = vm.message.likes.indexOf(userId);
                    if (unlikedByIndex >= 0) {
                        let updatedMessage = _.cloneDeep(_.pick(vm.message, 'id', 'discussion',
                            'text', 'likes', 'is_answer'));
                        updatedMessage.likes.splice(unlikedByIndex, 1);
                        vm.onUpdate({'message': updatedMessage});
                    }
                };

                vm.acceptAsAnswer = function (accepted) {
                    let updatedMessage = _.cloneDeep(_.pick(vm.message, 'id', 'discussion',
                            'text', 'likes', 'is_answer'));
                    updatedMessage.is_answer = accepted;
                    vm.onUpdate({'message': updatedMessage});
                };

                vm.$onInit = function () {
                    userService.getCurrentUser().then(function (user) {
                        vm.currentUser = user;
                    });
                };

            }]
        });
})();