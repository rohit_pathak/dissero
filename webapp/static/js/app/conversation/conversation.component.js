(function () {
    angular.module('dissero')
        .component('conversation', {
            controllerAs: 'vm',
            bindings: {
                conversationId: '<'
            },
            template: `
            <div class="row">
                <div class="col-sm-8">
                    <discussion ng-repeat="d in vm.conversation.discussions | orderBy:'created_timestamp'" 
                                discussion="d"
                                on-update-discussion="vm.updateDiscussion(discussion)"></discussion>
                    <button class="btn btn-lg btn-secondary" ng-click="vm.showNewDiscussionForm = true"
                            ng-show="!vm.showNewDiscussionForm">
                        <i class="far fa-fw fa-comment-alt-exclamation"></i> New related discussion
                    </button>
                    <div ng-show="vm.showNewDiscussionForm" class="card p-2">
                        <h5 class="pb-1 mb-2">New Discussion</h5>
                        <form ng-submit="vm.createDiscussion()">
                            <div><small>Type</small></div>
                            <div class="form-group mb-3">
                                <div class="custom-control custom-radio">
                                  <input type="radio" id="discussion-category-question" name="category" 
                                         ng-model="vm.newDiscussion.category" value="QUESTION" 
                                         class="custom-control-input" required>
                                  <label class="custom-control-label" for="discussion-category-question">Question</label>
                                </div>
                                <div class="custom-control custom-radio">
                                  <input type="radio" id="discussion-category-open" name="category" 
                                         ng-model="vm.newDiscussion.category" value="GENERAL" 
                                         class="custom-control-input" required>
                                  <label class="custom-control-label" for="discussion-category-open">Open</label>
                                </div>
                            </div>
                            <div><small>Title</small></div>
                            <input ng-model="vm.newDiscussion.title" class="form-control mb-3" required>
                            <div><small>Details</small></div>
                            <trix-editor angular-trix ng-if="true" ng-model="vm.newDiscussion.details" 
                                         class="trix-content" placeholder="Details..." required></trix-editor>
                            <div class="mt-2">
                                <button type="submit" class="btn btn-success">Create</button>
                                <button class="btn btn-secondary" ng-click="vm.showNewDiscussionForm = false">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h5>Summary</h5>
                    <div ng-repeat="d in vm.summary | toArray | orderBy: 'created_timestamp'">
                        <h6><u>{{d.title}}</u></h6>
                        <ul>
                            <li ng-bind-html="m.text" ng-repeat="m in d.messages | toArray | orderBy: 'created_timestamp'"
                                ng-class="m.is_answer ? 'text-success' : 'text-warning'">
                            </li>
                        </ul>
                    </div>    
                </div>
            </div>
            `,
            controller: ['$scope', '$state', '$websocket', '$location', '$anchorScroll',
                'conversationService', function ($scope, $state, $websocket, $location,
                                                 $anchorScroll, conversationService) {

                    let vm = this;
                    vm.conversation = null;
                    vm.summary = null;
                    vm.newDiscussion = {};
                    vm.discussionMap = {};
                    vm.messageMap = {};
                    vm.showNewDiscussionForm = false;
                    vm.socket = null;

                    vm.createDiscussion = function () {
                        vm.newDiscussion.conversation = vm.conversation.id;
                        let data = {
                            'resource': 'discussions',
                            'action': 'create',
                            'payload': vm.newDiscussion
                        };
                        vm.socket.send(data);
                        vm.newDiscussion = {};
                        vm.showNewDiscussionForm = false;
                    };

                    vm.updateDiscussion = function (discussion) {
                        let data = {
                            'resource': 'discussions',
                            'action': 'update',
                            'payload': _.pick(discussion, 'id', 'title', 'details', 'likes', 'conversation', 'category')
                        };
                        vm.socket.send(data);
                    };

                    vm.processSocketData = function (data) {
                        if (data.resource === 'messages') {
                            if (data.action === 'create') vm.receiveNewMessage(data.payload);
                            else if (data.action === 'update') vm.receiveUpdatedMessage(data.payload);
                        }
                        else if (data.resource === 'discussions') {
                            if (data.action === 'create') vm.receiveNewDiscussion(data.payload);
                            else if (data.action === 'update') vm.receiveUpdatedDiscussion(data.payload);
                        }
                    };

                    vm.receiveNewMessage = function (message) {
                        vm.discussionMap[message.discussion].messages.push(message);
                        vm.messageMap[message.id] = message;
                    };

                    vm.receiveNewDiscussion = function (discussion) {
                        vm.conversation.discussions.push(discussion);
                        vm.discussionMap[discussion.id] = discussion;
                        vm.summary = generateSummary(vm.conversation);
                    };

                    vm.receiveUpdatedDiscussion = function (updatedDiscussion) {
                        if (!(updatedDiscussion.id in vm.discussionMap)) return;
                        let discussion = vm.discussionMap[updatedDiscussion.id];
                        discussion.likes = updatedDiscussion.likes;
                        discussion.title = updatedDiscussion.title;
                        discussion.details = updatedDiscussion.details;
                        Object.assign(vm.summary[updatedDiscussion.id],
                            _.pick(updatedDiscussion, 'id', 'title', 'category', 'created_timestamp'));
                        $scope.$digest();
                    };

                    vm.receiveUpdatedMessage = function (updatedMessage) {
                        if (!(updatedMessage.id in vm.messageMap)) return;
                        let message = vm.messageMap[updatedMessage.id];
                        message.likes = updatedMessage.likes;
                        message.text = updatedMessage.text;
                        message.is_answer = updatedMessage.is_answer;
                        message.is_liked_by_instructor = updatedMessage.is_liked_by_instructor;
                        if (message.is_answer || message.is_liked_by_instructor) {
                            vm.summary[message.discussion].messages[message.id] = message;
                        } else if (message.id in vm.summary[message.discussion].messages) {
                            let messages = vm.summary[message.discussion].messages;
                            delete messages[message.id];
                        }
                        $scope.$digest();
                    };

                    vm.$onInit = function () {
                        conversationService.getConversation(vm.conversationId).then(function (data) {
                            vm.conversation = data;
                            vm.conversation.discussions.forEach(d => {
                                vm.discussionMap[d.id] = d;
                                d.messages.forEach(m => {
                                    vm.messageMap[m.id] = m;
                                });
                            });
                            vm.summary = generateSummary(vm.conversation);
                            const scheme = $location.protocol() === 'https' ? 'wss' : 'ws';
                            const url = `${scheme}://${$location.host()}:${$location.port()}/ws/conversations/${vm.conversation.id}/`;
                            vm.socket = $websocket(url);
                            vm.socket.onMessage(function (rawMessage) {
                                // console.log('raw message: ', rawMessage);
                                let data = JSON.parse(rawMessage.data);
                                vm.processSocketData(data);
                            });
                            vm.socket.onError(function (e) {
                                console.log(e);
                            });

                            if ($state.params.messageId !== null) {
                                vm.goTo('message-' + $state.params.messageId);
                            } else if ($state.params.discussionId !== null) {
                                vm.goTo('discussion-' + $state.params.discussionId);
                            }
                        });
                    };

                    vm.$onDestroy = function () {
                        vm.socket.close(true);
                    };

                    vm.goTo = function (hash) {
                        $location.hash(hash);
                        $anchorScroll();
                    };

                    function generateSummary(conversation) {
                        let summary = vm.summary || {};
                        conversation.discussions
                            .filter(d => !(d.id in (vm.summary || {})))
                            .forEach(d => {
                                summary[d.id] = _.pick(d, 'id', 'title', 'category', 'created_timestamp');
                                let messageMap = {};
                                d.messages
                                    .filter(m => m.is_liked_by_instructor || m.is_answer)
                                    .forEach(m => messageMap[m.id] = m);
                                summary[d.id].messages = messageMap;
                            });
                        return summary;
                    }
                }]
        })
})();