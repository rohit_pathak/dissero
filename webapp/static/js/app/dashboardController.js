(function () {
    angular.module('dissero')
        .controller('DashboardController', [function () {
            let vm = this;
            vm.user = 'Hello user!';
            vm.squares = [1, 2, 3, 4].map(i => i * i);
        }]);
})();