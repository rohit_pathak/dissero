(function () {
    let app = angular.module('dissero', ['ngSanitize', 'ui.router', 'ngWebSocket', 'angular-toArrayFilter', 'angularTrix'])
        .config(['$httpProvider', function ($httpProvider) {
            $httpProvider.defaults.xsrfCookieName = 'csrftoken';
            $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        }])
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            let states = [
                {
                    name: 'home',
                    url: '',
                    component: 'classroomList',
                },
                {
                    name: 'home2',
                    url: '/',
                    component: 'classroomList'
                },
                {
                    name: 'classroom',
                    url: '/classrooms/{classroomId}',
                    component: 'classroom',
                    resolve: {
                        classroomId: function ($stateParams) {
                            return $stateParams.classroomId;
                        }
                    }
                },
                {
                    name: 'classroom.conversations',
                    url: '/conversations',
                    component: 'conversationList'
                },
                {
                    name: 'classroom.conversation',
                    url: '/conversations/{conversationId}',
                    component: 'conversation',
                    params: {
                        messageId: null,
                        discussionId: null
                    },
                    resolve: {
                        conversationId: function ($stateParams) {
                            return $stateParams.conversationId;
                        }
                    }
                }
            ];

            $urlRouterProvider.when('/classrooms/:classroomId', '/classrooms/:classroomId/conversations');
            $urlRouterProvider.otherwise('/');

            states.forEach(function (state) {
                $stateProvider.state(state);
            });
        }]);
})();