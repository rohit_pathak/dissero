(function () {
    angular.module('dissero')
        .component('classroomList', {
            controllerAs: 'vm',
            template: `
                <div ng-show="vm.instructorClassrooms.length" class="mb-4">
                    <h5 class="pb-2 border-bottom">
                        Classrooms as Instructor 
                        <button class="btn btn-success btn-sm ml-2" ng-click="vm.showCreateClassroomForm = true"
                            ng-show="vm.currentUser.is_instructor">
                        <i class="fas fa-plus"></i> New Classroom</button>
                    </h5>
                    <div ng-show="vm.showCreateClassroomForm" class="card p-2 mb-3">
                        <h5 class="pb-1 mb-2">New Classroom</h5>
                        <form ng-submit="vm.createClassroom()">
                            <fieldset ng-disabled="vm.isSaving">
                                <input type="text" ng-model="vm.newClassroom.name" class="form-control" placeholder="Classroom name">
                                <div class="mt-2">
                                    <button type="submit" class="btn btn-success">Create</button>
                                    <button class="btn btn-secondary" ng-click="vm.showCreateClassroomForm = false">Cancel</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <h3 ng-repeat="c in vm.instructorClassrooms" ng-show="!vm.showCreateClassroomForm">
                        <a ui-sref="classroom({classroomId: c.id})" href>{{ c.name }}</a>
                    </h3>
                </div>
                <div ng-show="vm.studentClassrooms.length">
                    <h5>Classrooms as Student</h5>
                    <h3 ng-repeat="c in vm.studentClassrooms">
                        <a ui-sref="classroom({classroomId: c.id})" href>{{ c.name }}</a>
                    </div>
                </div>
            `,
            controller: ['classroomService', 'userService', function (classroomService, userService) {
                let vm = this;
                vm.instructorClassrooms = null;
                vm.studentClassrooms = null;
                vm.showCreateClassroomForm = false;
                vm.newClassroom = {};
                vm.currentUser = {};

                vm.$onInit = function () {
                    classroomService.getClassrooms('instructor').then(function (data) {
                        vm.instructorClassrooms = data;
                        console.log(vm.instructorClassrooms);
                    });
                    classroomService.getClassrooms('student').then(function (data) {
                        vm.studentClassrooms = data;
                        console.log(vm.studentClassrooms);
                    });
                    userService.getCurrentUser().then(function (user) {
                        console.log('User: ', user);
                        vm.currentUser = user;
                    })
                };

                vm.createClassroom = function () {
                    vm.isSaving = true;
                    console.log("creating classroom");
                    classroomService.createClassroom(vm.newClassroom).then(function (classroom) {
                        console.log(classroom);
                        vm.instructorClassrooms.push(classroom);
                        vm.isSaving = false;
                        vm.showCreateClassroomForm = false;
                    }, function (err) {
                        vm.isSaving = false;
                        console.error(err);
                    });
                }
            }]
        });
})();