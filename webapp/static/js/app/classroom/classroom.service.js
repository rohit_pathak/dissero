(function () {
    angular.module('dissero')
        .factory('classroomService', ['$http', '$httpParamSerializer', function ($http, $httpParamSerializer) {
            let factory = {};
            factory.getClassrooms = function (enrolledAs) {
                if (enrolledAs) {
                    return $http.get('/api/classrooms?enrolled_as=' + enrolledAs).then(function (response) {
                        return response.data;
                    });
                }
                return $http.get('/api/classrooms?enrolled_as=student').then(function (response) {
                    return response.data;
                })
            };

            factory.getClassroom = function (id) {
                return $http.get('/api/classrooms/' + id).then(function (response) {
                    return response.data;
                });
            };

            factory.search = function(id, term) {
                let url = '/api/classrooms/' + id + '/search?' + $httpParamSerializer({'term': term});
                return $http.get(url).then(function (response) {
                    return response.data;
                });
            };

            factory.createClassroom = function (classroom) {
                return $http.post('/api/classrooms/', classroom).then(function (response) {
                    return response.data;
                });
            };

            return factory;
        }]);
})();