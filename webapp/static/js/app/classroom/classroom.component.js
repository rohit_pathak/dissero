(function () {
    angular.module('dissero')
        .component('classroom', {
            controllerAs: 'vm',
            bindings: {
                classroomId: '<'
            },
            template: `
                <h5 class="border-bottom mb-3 pb-2">
                    <a class="btn btn-secondary" ui-sref="classroom.conversations({classroomId: vm.classroomId})"><i class="fas fa-home"></i></a> 
                    {{ vm.classroom.name }}
                </h5>
                <ui-view></ui-view>
            `,
            controller: ['classroomService', function (classroomService) {
                let vm = this;
                vm.classroom = null;

                vm.$onInit = function () {
                    classroomService.getClassroom(vm.classroomId).then(function (data) {
                        vm.classroom = data;
                    });
                };
            }]
        })
})();