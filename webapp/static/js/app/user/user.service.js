(function () {
    angular.module('dissero')
        .factory('userService', ['$http', function (http) {
            let factory = {};

            factory.getCurrentUser = function () {
                return http.get('/api/users/current/', {cache: true}).then(function (response) {
                    return response.data;
                });
            };

            return factory;
        }]);
})();