from django.contrib.auth import get_user_model, authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from useraccount.forms import LoginForm

User = get_user_model()


def login_request(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('home-page'))

    if request.method == 'POST':  # user is trying to log in
        next_url = request.GET.get('next')
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']  # our username is the email
            password = form.cleaned_data['password']
            user = authenticate(username=email, password=password)
            login(request, user)
            if next_url is not None:
                return HttpResponseRedirect(next_url)
            else:
                return HttpResponseRedirect(reverse('home-page'))
        else:  # form is not valid
            return render(request, 'useraccount/login.html', {'form': form})

    elif request.method == 'GET':
        next_url = request.GET.get('next')
        return render(request, 'useraccount/login.html', {'next_url': next_url})

    else:
        return HttpResponseRedirect(reverse('home-page'))


def logout_request(request):
    if request.user.is_authenticated:
        logout(request)
        return render(request, 'useraccount/logout-success.html')
    else:
        return HttpResponseRedirect(reverse('dashboard-page'))


@login_required
def dashboard(request):
    return render(request, 'useraccount/dashboard.html')
