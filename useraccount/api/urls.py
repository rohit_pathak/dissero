from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('<int:pk>/', views.UserDetail.as_view(), name='user-detail'),
    path('current/', views.CurrentUserDetail.as_view(), name='current-user-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
