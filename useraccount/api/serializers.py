from rest_framework import serializers

from useraccount.models import User


class UserSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source='get_full_name', read_only=True)
    is_instructor = serializers.SerializerMethodField()

    def get_is_instructor(self, obj):
        return obj.groups.filter(name=User.INSTRUCTORS_GROUP).exists()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'full_name', 'is_instructor')
