from django.core.management.base import BaseCommand

from useraccount.models import User


class Command(BaseCommand):
    help = 'Initializes an "admin" superuser if it does not exists'

    def add_arguments(self, parser):
        parser.add_argument('--email', dest='email', required=True, help='Email of the new superuser')
        parser.add_argument('--password', dest='password', required=True, help='Password of the new superuser')

    def handle(self, *args, **options):
        email, password = options['email'].strip(), options['password'].strip()
        if User.objects.filter(email=email).exists():
            self.stdout.write(self.style.ERROR('User already exists. Skipping'))
        else:
            User.objects.create_superuser(email=email, password=password)
            self.stdout.write(self.style.SUCCESS('Successfully created new superuser with email %s' % email))
