from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from django.views import View

NG_TEMPLATES = {

}


class ClassroomNgTemplate(LoginRequiredMixin, View):
    def get(self, request, template_name):
        if template_name not in NG_TEMPLATES:
            raise PermissionDenied()
        return render(request, 'classroom/' + template_name)
