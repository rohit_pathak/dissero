from django.urls import path

from . import views

app_name = 'classroom'
urlpatterns = [
    path('ng/<slug:template_name>/', views.ClassroomNgTemplate.as_view(), name='classroom-ng-template'),
]
