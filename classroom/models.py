from django.conf import settings
from django.db import models


class Classroom(models.Model):
    name = models.CharField(max_length=100)
    instructors = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='instructor_classrooms')
    students = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='student_classrooms', blank=True)
    is_active = models.BooleanField(default=True)

    def has_member(self, user):
        return self.instructors.filter(pk=user.pk).exists() or self.students.filter(pk=user.pk).exists()

    def __str__(self):
        return self.name
