from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('', views.ClassroomList.as_view(), name='classroom-list'),
    path('<int:pk>/', views.ClassroomDetail.as_view(), name='classroom-detail'),
    path('<int:pk>/conversations/', views.ClassroomConversationList.as_view(), name='classroom-conversation-list'),
    path('<int:pk>/search/', views.ClassroomSearch.as_view(), name='classroom-search'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
