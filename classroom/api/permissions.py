from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied


class IsClassroomMember(permissions.BasePermission):
    def has_object_permission(self, request, view, classroom):
        return classroom.has_member(request.user)


class IsClassroomInstructorOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, classroom):
        if request.method in permissions.SAFE_METHODS and classroom.has_member(request.user):
            return True
        return classroom.instructors.filter(pk=request.user.pk).exists()


def classroom_member_or_forbidden(user, classroom):
    if not classroom.has_member(user):
        raise PermissionDenied(detail='You are not enrolled in this classroom')
