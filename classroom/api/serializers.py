from rest_framework import serializers

from classroom.models import Classroom
from useraccount.api.serializers import UserSerializer


class ClassroomListSerializer(serializers.HyperlinkedModelSerializer):
    instructors = UserSerializer(many=True, read_only=True)

    class Meta:
        model = Classroom
        fields = ('id', 'url', 'name', 'instructors', 'is_active')
        read_only_fields = ['instructors', 'is_active']


class ClassroomDetailSerializer(serializers.HyperlinkedModelSerializer):
    instructors_detail = UserSerializer(many=True, read_only=True, source='instructors')
    students_detail = UserSerializer(many=True, read_only=True, source='students')

    class Meta:
        model = Classroom
        fields = ('id', 'url', 'name', 'instructors', 'students', 'is_active',
                  'instructors_detail', 'students_detail')


class SearchResultSerializer(serializers.Serializer):
    category = serializers.CharField()
    conversation_id = serializers.IntegerField()
    discussion_id = serializers.IntegerField()
    message_id = serializers.IntegerField(required=False)
    url = serializers.URLField(required=False)
    text = serializers.CharField()
