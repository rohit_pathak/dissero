from bs4 import BeautifulSoup
from django.contrib.postgres.search import SearchQuery, SearchVector
from rest_framework import generics
from rest_framework.exceptions import ParseError, PermissionDenied
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from classroom.api.permissions import IsClassroomMember, IsClassroomInstructorOrReadOnly
from classroom.api.serializers import ClassroomListSerializer, ClassroomDetailSerializer, SearchResultSerializer
from classroom.models import Classroom
from conversation.api.serializers import ConversationSummarySerializer
from conversation.models import Discussion, Message
from useraccount.models import User


class ClassroomList(generics.ListCreateAPIView):
    serializer_class = ClassroomListSerializer
    permission_classes = (IsAuthenticated, IsClassroomMember)

    def get_queryset(self):
        enrolled_as = self.request.query_params.get('enrolled_as', None)
        if enrolled_as is None and enrolled_as != 'instructor' and enrolled_as != 'student':
            raise ParseError(detail='Please provide the enrolled_as query parameter.')
        if enrolled_as == 'instructor':
            return self.request.user.instructor_classrooms.all()
        return self.request.user.student_classrooms.all()

    def perform_create(self, serializer):
        if not self.request.user.groups.filter(name=User.INSTRUCTORS_GROUP).exists():
            raise PermissionDenied(detail='You cannot create a classroom as you are not an instructor')
        serializer.save(instructors=[self.request.user])


class ClassroomDetail(generics.RetrieveUpdateAPIView):
    serializer_class = ClassroomDetailSerializer
    permission_classes = (IsAuthenticated, IsClassroomInstructorOrReadOnly)
    queryset = Classroom.objects.all()


class ClassroomConversationList(generics.ListAPIView):
    serializer_class = ConversationSummarySerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        classroom = get_object_or_404(Classroom, pk=self.kwargs.get('pk'))
        if not classroom.has_member(self.request.user):
            raise PermissionDenied(detail='You are not a member of this classroom')
        return classroom.conversation_set.all()


class ClassroomSearch(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk, format=None):
        classroom = get_object_or_404(Classroom, pk=pk)
        if not classroom.has_member(request.user):
            raise PermissionDenied(detail='You are not a member of this classroom')
        term = self.request.query_params.get('term', None)
        if term is None:
            raise ParseError(detail='No search term provided')

        search_query = SearchQuery(term, config='english')
        discussions = Discussion.objects.annotate(
            search=SearchVector('title', 'details', config='english')
        ).filter(conversation__classroom=classroom, search=search_query)
        messages = Message.objects.annotate(
            search=SearchVector('text', config='english')
        ).filter(discussion__conversation__classroom=classroom, search=search_query)

        result = SearchResultSerializer(
            [SearchResult(category=SearchResult.DISCUSSION, text=d.title,
                          conversation_id=d.conversation.id,
                          discussion_id=d.id) for d in discussions] +
            [SearchResult(category=SearchResult.MESSAGE, text=BeautifulSoup(m.text).get_text(),
                          conversation_id=m.discussion.conversation.id,
                          discussion_id=m.discussion.id, message_id=m.id) for m in messages],
            many=True)
        return Response(result.data)


class SearchResult:
    DISCUSSION = 'DISCUSSION'
    MESSAGE = 'MESSAGE'

    def __init__(self, category, text, conversation_id=None, discussion_id=None,
                 message_id=None, url=None):
        self.category = category
        self.text = text
        self.conversation_id = conversation_id
        self.discussion_id = discussion_id
        self.message_id = message_id
        self.url = url
